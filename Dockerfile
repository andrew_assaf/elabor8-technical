#pullin golang docker image
FROM golang:alpine

# setting the work directory as src, any command after this will run in /src
WORKDIR /src

# copying all files from src directory from local src to /src on docker image
COPY src /src

# create execcutable file with name main
RUN go build -o main .

# run the main execuatable
CMD ["./main"]




