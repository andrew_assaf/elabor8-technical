// Note: Looks like the facts API( https://cat-fact.herokuapp.com/facts) has changed and there is no field upvotes, so I am getting the user name and fact text using the API and printing them out

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type Facts []struct {
	Status struct {
		Verified  bool   `json:"verified"`
		SentCount int    `json:"sentCount"`
		Feedback  string `json:"feedback"`
	} `json:"status"`
	Type      string    `json:"type"`
	Deleted   bool      `json:"deleted"`
	ID        string    `json:"_id"`
	User      string    `json:"user"`
	Text      string    `json:"text"`
	V         int       `json:"__v"`
	Source    string    `json:"source"`
	UpdatedAt time.Time `json:"updatedAt"`
	CreatedAt time.Time `json:"createdAt"`
	Used      bool      `json:"used"`
}

type Fact struct {
	Status struct {
		Verified  bool `json:"verified"`
		SentCount int  `json:"sentCount"`
	} `json:"status"`
	Type    string `json:"type"`
	Deleted bool   `json:"deleted"`
	ID      string `json:"_id"`
	User    struct {
		Name struct {
			First string `json:"first"`
			Last  string `json:"last"`
		} `json:"name"`
		Photo string `json:"photo"`
		ID    string `json:"_id"`
	} `json:"user"`
	Text      string    `json:"text"`
	V         int       `json:"__v"`
	Source    string    `json:"source"`
	UpdatedAt time.Time `json:"updatedAt"`
	CreatedAt time.Time `json:"createdAt"`
	Used      bool      `json:"used"`
}

func main() {
	fmt.Println("Calling API...")
	client := &http.Client{}
	// prepare a get call on /facts to get all facts
	req, err := http.NewRequest("GET", "https://cat-fact.herokuapp.com/facts", nil)
	// checking if errors
	if err != nil {
		fmt.Print(err.Error())
	}
	// set headers to recieve the resposnse in json format
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	// makeing the get call
	resp, err := client.Do(req)
	if err != nil {
		fmt.Print(err.Error())
	}
	// reading from the response.body
	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Print(err.Error())
	}

	// creating a variable to store all the facts
	var responseObject Facts
	// unmarshalling (reading json and converting into struct) the json into the struct
	json.Unmarshal(bodyBytes, &responseObject)

	//loop through the facts to get each fact detail through the fact ID
	for _, fact := range responseObject {
		req, err := http.NewRequest("GET", "https://cat-fact.herokuapp.com/facts/"+fact.ID, nil)
		if err != nil {
			fmt.Print(err.Error())
		}
		req.Header.Add("Accept", "application/json")
		req.Header.Add("Content-Type", "application/json")
		resp, err := client.Do(req)

		if err != nil {
			fmt.Print(err.Error())
		}
		defer resp.Body.Close()
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Print(err.Error())
		}
		//creating variable to read each fact =
		var responseFact Fact
		//converting json into a (fact)struct
		json.Unmarshal(bodyBytes, &responseFact)
		//printing out detials
		fmt.Println(responseFact.User.Name.First, " ", responseFact.User.Name.Last, ": ", responseFact.Text)

	}

}
